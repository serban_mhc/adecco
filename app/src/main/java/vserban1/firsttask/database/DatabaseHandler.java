package vserban1.firsttask.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import vserban1.firsttask.model.ImageItem;

/**
 * Created by vserban1 on 12/11/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "urlManager";

    private static final String TABLE_IMAGE_URL = "url";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "imageUrl";
    private static final String KEY_PH_NO = "thumbUrl";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_IMAGE_TABLE = "CREATE TABLE " + TABLE_IMAGE_URL + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT" + ")";
        db.execSQL(CREATE_IMAGE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE_URL);

        // Create tables again
        onCreate(db);
    }

    public void addImage(ImageItem images) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, images.getImageURL());
        values.put(KEY_PH_NO, images.getthumbURL());

        // Inserting Row
        db.insert(TABLE_IMAGE_URL, null, values);
        db.close();
    }

    public ImageItem getImage(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_IMAGE_URL, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ImageItem images = new ImageItem(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        return images;
    }

    public List<ImageItem> getAllImages() {
        List<ImageItem> imageList = new ArrayList<ImageItem>();
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGE_URL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ImageItem image = new ImageItem();
                image.setID(Integer.parseInt(cursor.getString(0)));
                image.setImageURL(cursor.getString(1));
                image.setthumbURL(cursor.getString(2));
                imageList.add(image);
            } while (cursor.moveToNext());
        }

        return imageList;
    }

}
