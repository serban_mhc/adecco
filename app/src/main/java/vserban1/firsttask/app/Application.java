package vserban1.firsttask.app;


import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import vserban1.firsttask.R;
import vserban1.firsttask.interceptor.TokenInterceptor;
import vserban1.firsttask.retrofit.ApiService;


public class Application extends android.app.Application {
    private static final String TAG = Application.class.getSimpleName();
    private static Application instance;
    private static OkHttpClient sOkHttpClient;
    private static ApiService sApiService;

    public static synchronized Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        Picasso picasso = new Picasso.Builder(this)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        Log.d(TAG, "onImageLoadFailed: ");
                        exception.printStackTrace();
                    }
                }).build();

        Picasso.setSingletonInstance(picasso);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        sOkHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(new TokenInterceptor(this))
                .addInterceptor(logging)
                .build();
        instance = this;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .client(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        sApiService = retrofit.create(ApiService.class);
    }

    public static ApiService getApiService() {
        return sApiService;
    }


}
