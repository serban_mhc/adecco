package vserban1.firsttask.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class ImageItem implements Parcelable {

    int id;
    @SerializedName("tmb_url")
    private String thumbURL;
    @SerializedName("img_url")
    private String imageURL;


    public ImageItem() {
    }

    public ImageItem(Parcel in) {
        this.thumbURL = in.readString();
        this.imageURL = in.readString();
    }

    public ImageItem(int id, String imageUrl, String thumbUrl){
        this.id = id;
        this.imageURL = imageUrl;
        this.thumbURL = thumbUrl;
    }
    public ImageItem( String imageUrl, String thumbUrl){
        this.imageURL = imageUrl;
        this.thumbURL = thumbUrl;
    }

    public String getthumbURL() {
        return thumbURL;
    }

    public void setthumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getID(){
        return this.id;
    }

    public void setID(int id){
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumbURL);
        dest.writeString(this.imageURL);

    }

    public static final Creator<ImageItem> CREATOR = new Creator<ImageItem>() {
        @Override
        public ImageItem createFromParcel(Parcel source) {
            return new ImageItem(source);
        }

        @Override
        public ImageItem[] newArray(int size) {
            return new ImageItem[size];
        }
    };


}