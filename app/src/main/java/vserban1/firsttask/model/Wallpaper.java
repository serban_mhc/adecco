package vserban1.firsttask.model;

import java.util.List;


public class Wallpaper {
    private List<ImageItem> wallpapers;

    public List<ImageItem> getWallpapers() {
        return wallpapers;
    }

    public void setWallpapers(List<ImageItem> wallpapers) {
        this.wallpapers = wallpapers;
    }
}
