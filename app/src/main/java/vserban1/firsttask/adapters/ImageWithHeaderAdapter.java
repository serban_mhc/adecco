package vserban1.firsttask.adapters;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import vserban1.firsttask.R;
import vserban1.firsttask.model.ImageItem;


public class ImageWithHeaderAdapter extends BaseAdapter<ImageItem> implements ImageListItemView.Listener {

    public static final int TYPE_HEADER=0;
    private static final String TAG = "info";

    @Override
    public int getItemCount() {
        return super.getItemCount()+1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (TYPE_HEADER==viewType) {
            HeaderListWallpaper view = new HeaderListWallpaper(parent.getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            return new ViewHolder(view);
        } else {
            ImageListItemView view = new ImageListItemView(parent.getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            view.setListener(this);
            view.setColor(view.getContext().getDrawable(R.drawable.cell_border_yellow));
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseAdapter.ViewHolder holder, int position) {
        if(TYPE_HEADER==position){
            Log.d(TAG, "onBindViewHolder: info");
        }else {
            super.onBindViewHolder(holder, position-1);
        }
    }

    @Override
    public void OnItemClick(ImageItem item, View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getImageURL()));
        if (browserIntent.resolveActivity(view.getContext().getPackageManager()) != null) {
            view.getContext().startActivity(browserIntent);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0) {
            return TYPE_HEADER;
        }
        return 1;
    }

}



