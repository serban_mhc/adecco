package vserban1.firsttask.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import vserban1.firsttask.R;
import vserban1.firsttask.model.ImageItem;


public class ImageListItemView extends FrameLayout implements BaseAdapter.Assignable<ImageItem> {
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.rlImage)
    RelativeLayout rlImage;
    private Listener mListener;
    private Drawable mDrawable;
    private RecyclerView.LayoutManager mLayout;


    public ImageListItemView(@NonNull Context context) {
        super(context);
        Innit();
    }

    private void Innit() {
        LayoutInflater.from(getContext())
                .inflate(R.layout.image_list_item_view, this, true);
        ButterKnife.bind(this);
    }

    public ImageListItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Innit();
    }

    public interface Listener {
        public void OnItemClick(ImageItem item, View view);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void setColor(Drawable drawable) {
        mDrawable = drawable;
    }

    public ImageListItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Innit();
    }

    @Override
    public void setObject(final ImageItem object) {

        Picasso.with(getContext())
                .load(object.getthumbURL())
                .fit()
                .centerCrop()
                .into(imageView);
        if (mListener != null) {
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.OnItemClick(object, v);
                }
            });
        }
        rlImage.setBackground(mDrawable);

    }

}


