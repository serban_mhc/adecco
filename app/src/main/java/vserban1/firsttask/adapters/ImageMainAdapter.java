package vserban1.firsttask.adapters;

import android.view.ViewGroup;

import vserban1.firsttask.R;
import vserban1.firsttask.model.ImageItem;


public class ImageMainAdapter extends BaseAdapter<ImageItem> {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageListItemView view = new ImageListItemView(parent.getContext());
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        view.setColor(view.getContext().getDrawable(R.drawable.image_no_margins));
        return new ViewHolder(view);
    }

}



