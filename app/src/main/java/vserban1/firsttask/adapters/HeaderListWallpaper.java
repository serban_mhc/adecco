package vserban1.firsttask.adapters;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import vserban1.firsttask.R;
import vserban1.firsttask.model.ImageItem;


class HeaderListWallpaper extends FrameLayout implements BaseAdapter.Assignable<ImageItem> {

    public HeaderListWallpaper(@NonNull Context context) {
        super(context);
        Innit();
    }

    private void Innit() {
        LayoutInflater.from(getContext())
                .inflate(R.layout.ui_header_wallpaper, this, true);
    }

    public HeaderListWallpaper(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Innit();
    }

    public HeaderListWallpaper(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Innit();
    }

    @Override
    public void setObject(ImageItem object) {

    }


}


