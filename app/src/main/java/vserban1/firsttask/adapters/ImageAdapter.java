package vserban1.firsttask.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import vserban1.firsttask.R;
import vserban1.firsttask.model.ImageItem;


public class ImageAdapter extends BaseAdapter<ImageItem> implements ImageListItemView.Listener {
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageListItemView view = new ImageListItemView(parent.getContext());
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        view.setListener(this);
        view.setColor(view.getContext().getDrawable(R.drawable.cell_border_blue));
        return new BaseAdapter.ViewHolder(view);

    }


    @Override
    public void OnItemClick(ImageItem item, View v) {
        Toast.makeText(v.getContext(), item.getImageURL(), Toast.LENGTH_LONG).show();
    }
}



