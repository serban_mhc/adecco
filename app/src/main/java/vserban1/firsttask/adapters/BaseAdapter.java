package vserban1.firsttask.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseAdapter<T>
        extends RecyclerView.Adapter<BaseAdapter.ViewHolder> {

    protected List<T> mObjectList = new ArrayList<>();

    @Override
    public int getItemCount() {
        if (mObjectList == null) {
            return 0;
        }
        return mObjectList.size();
    }

    public void setObjectList(List<T> objectList) {
        mObjectList.clear();
        mObjectList.addAll(objectList);
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(final BaseAdapter.ViewHolder holder, final int position) {
        //noinspection unchecked
        ((Assignable<T>) holder.itemView).setObject(mObjectList.get(position));


    }


    /**
     * View holder pattern.
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * holder
         *
         * @param itemView view to hold.
         */
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface Assignable<T> {
        void setObject(T object);
    }


}
