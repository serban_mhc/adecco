package vserban1.firsttask.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class GsonHelper {
    private static GsonHelper sInstance;

    private final Gson mGson;

    public static GsonHelper getInstance() {
        if (sInstance == null) {
            sInstance = new GsonHelper();
        }
        return sInstance;
    }

    public static Gson getGson() {
        return getInstance().mGson;
    }

    private GsonHelper() {
        mGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }
}
