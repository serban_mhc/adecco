package vserban1.firsttask.retrofit;

import android.content.Context;

import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;
import rx.Subscription;



public class RetrofitUtils {
    public static void releaseSubscription(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    public static boolean handleError(Context context, Throwable e) {
        if (e instanceof SocketTimeoutException) {
            return true;
        }
        return RetrofitUtils.handleGenericError(context, e);
    }

    private static boolean handleGenericError(Context context, Throwable e) {
        if (e != null && e instanceof HttpException) {
            Response<?> response = ((HttpException) e).response();
            if (response != null && response.code() != 502 && response.code() != 420) {
                ResponseBody body = response.errorBody();
                if (body != null) {
                }
            }
        }
        return false;
    }
}
