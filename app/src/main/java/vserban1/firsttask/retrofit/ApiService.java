package vserban1.firsttask.retrofit;

import retrofit2.http.GET;
import rx.Observable;
import vserban1.firsttask.model.Wallpaper;



public interface ApiService {
    @GET("/api/android/wallpapers.json")
    Observable<Wallpaper> getImages();
}
