package vserban1.firsttask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import vserban1.firsttask.adapters.ImageMainAdapter;
import vserban1.firsttask.app.Application;
import vserban1.firsttask.fragments.ViewTwoRowsFragment;
import vserban1.firsttask.model.ImageItem;
import vserban1.firsttask.model.Wallpaper;
import vserban1.firsttask.retrofit.RetrofitUtils;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.rvListItems)
    RecyclerView rvImage;
    @BindView(R.id.container)
    RelativeLayout container;
    @BindView(R.id.loading)
    RelativeLayout loading;


    private ImageMainAdapter mAdapter;
    private ArrayList<ImageItem> mRecyclerList;
    private Subscription mSubscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        loading.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        setUI();

    }

    @OnClick(R.id.fragmentView)
    public void startFragment(View view) {
        container.setVisibility(View.GONE);
        ViewTwoRowsFragment fragment = ViewTwoRowsFragment.newInstance(mRecyclerList);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container2, fragment);
        transaction.addToBackStack(null);
        transaction.commit();


    }

    @OnClick(R.id.activityView)
    public void startActivity(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putParcelableArrayListExtra("picturesUrl", mRecyclerList);
        startActivity(intent);
    }

    private void setUI() {
        mRecyclerList = new ArrayList<>();
        mAdapter = new ImageMainAdapter();
        rvImage.setLayoutManager(new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false));
        rvImage.setAdapter(mAdapter);
        getImageUrl();

    }


    private void getImageUrl() {
        mSubscription = Application
                .getApiService()
                .getImages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onUrlRetrieved());

    }

    private Observer<? super Wallpaper> onUrlRetrieved() {
        return new Subscriber<Wallpaper>() {
            @Override
            public void onCompleted() {
                loading.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("", "err: ");
            }

            @Override
            public void onNext(Wallpaper urlResponse) {
                for (ImageItem as : urlResponse.getWallpapers()) {
                    mRecyclerList.add(as);
                }
                mAdapter.setObjectList(mRecyclerList);

            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RetrofitUtils.releaseSubscription(mSubscription);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().popBackStackImmediate()) {
            container.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }

    }
}
