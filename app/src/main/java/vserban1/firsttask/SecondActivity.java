package vserban1.firsttask;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vserban1.firsttask.adapters.ImageAdapter;
import vserban1.firsttask.database.DatabaseHandler;
import vserban1.firsttask.model.ImageItem;


public class SecondActivity extends AppCompatActivity {
    @BindView(R.id.rvListItems)
    RecyclerView rvImage;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    private Bundle bundle;
    private ImageAdapter mAdapter;
    private ArrayList<ImageItem> mRecyclerList;
    private Random mRandom = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
        mRecyclerList = new ArrayList<>();
        mRecyclerList = bundle.getParcelableArrayList("picturesUrl");
        getData();
        setUI();

    }


    private void setUI() {
        mAdapter = new ImageAdapter();
        rvImage.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false));
        rvImage.setAdapter(mAdapter);
        mAdapter.setObjectList(mRecyclerList);
    }

    @OnClick(R.id.fabAddImage)
    public void addNewImage(View v) {
        int position = 0;

        ImageItem imageList = new ImageItem();
        int index = mRandom.nextInt(mRecyclerList.size());
        String imageThumb=mRecyclerList.get(index).getthumbURL();
        String image=mRecyclerList.get(index).getImageURL();
        imageList.setthumbURL(imageThumb);
        imageList.setImageURL(image);
        saveImageToDB(image,imageThumb);
        mRecyclerList.add(position, imageList);
        mAdapter.notifyItemInserted(position);
        rvImage.scrollToPosition(position);
        mAdapter.setObjectList(mRecyclerList);

    }

    private void saveImageToDB(String image, String imageThumb) {
        DatabaseHandler db = new DatabaseHandler(this);
        db.addImage(new ImageItem(image, imageThumb));
    }

    private void getData(){
        DatabaseHandler db=new DatabaseHandler(this);
        List<ImageItem> imageNew = db.getAllImages();

        for (ImageItem cn : imageNew) {
            mRecyclerList.add(0,cn);
        }
    }
}
