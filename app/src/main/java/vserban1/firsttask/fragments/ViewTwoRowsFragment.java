package vserban1.firsttask.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import vserban1.firsttask.R;
import vserban1.firsttask.adapters.ImageWithHeaderAdapter;
import vserban1.firsttask.model.ImageItem;


public class ViewTwoRowsFragment extends Fragment {
    private View mFragmentView;
    @BindView(R.id.rvTwoRows)
    RecyclerView rvImage;
    private ArrayList<ImageItem> mRecyclerList;
    private ImageWithHeaderAdapter mAdapter;

    public ViewTwoRowsFragment() {

    }

    public static ViewTwoRowsFragment newInstance(ArrayList<ImageItem> list) {

        Bundle args = new Bundle();
        args.putParcelableArrayList("img", list);
        ViewTwoRowsFragment fragment = new ViewTwoRowsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecyclerList = new ArrayList<>();
        mRecyclerList = getArguments().getParcelableArrayList("img");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentView = inflater.inflate(R.layout.two_row_fragment, container, false);
        ButterKnife.bind(this, mFragmentView);

        setUI();
        return mFragmentView;
    }


    private void setUI() {
        mAdapter = new ImageWithHeaderAdapter();
        final GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(ImageWithHeaderAdapter.TYPE_HEADER == position){
                   return manager.getSpanCount();
                }
                return 1;
            }
        });
        rvImage.setLayoutManager(manager);
        rvImage.setAdapter(mAdapter);
        mAdapter.setObjectList(mRecyclerList);
    }


}
